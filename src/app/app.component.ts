import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

export class AppComponent implements OnInit{

    profileForm : FormGroup;
    submitted = false;
    success = false;

    constructor(private formBuilder: FormBuilder){ }
    
    ngOnInit(){
        this.profileForm = this.formBuilder.group({
            name : ['', Validators.required],
            email : ['',Validators.required],
            phone : ['',Validators.required]

        })
    }
    onSubmit(){
        this.submitted = true; 
    if(this.profileForm.invalid){
        return;
    }
    this.success = true;
    }
}

/*
export class AppComponent {
  title = 'angularform';
}*/
